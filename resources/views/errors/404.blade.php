
@extends('layouts.layout')

@section('title', 'Page Not Found (404)')


@section('content')
    <div class="container">

        <div class="alert alert-danger" role="alert">
            <h1 class="text-center">Page Not Found (404)</h1>
        </div>
    </div>
@endsection