<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');
//Route::get('/profile/{id}', 'HomeController@profile');
Route::get('/authorize', 'AuthorizeController@askUserAuthorization')->name("askUserAuthorization");
Route::post('/authorize/form', 'AuthorizeController@authorizeFormSubmit')->name("authorizeFormSubmit");
Route::post('/token/get-access-token', 'TokenController@getAccessToken')->name("getAccessToken");
Route::get('/resource/get-friends', 'ResourceController@getFriends');