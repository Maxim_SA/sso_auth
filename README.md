# Auth server


## Installation

Use [Composer](http://getcomposer.org/) to install this application:

    $ git clone https://Maxim_SA@bitbucket.org/Maxim_SA/sso_auth.git
    $ cd sso_auth
    $ curl -s http://getcomposer.org/installer | php
    $ ./composer.phar install
    $ npm i 
    $ npm run build:dev
    $ cp .env.example .env
    $ php artisan key:generate
    $ gunzip < sso_auth_demo.sql.gz | mysql -u<USER> sso_auth_demo -p
 
Update .env with database data (database name, user, password).

## Setup Apache virtual host

```
<VirtualHost *:80>
        ServerName sso_auth.local
        ServerAdmin webmaster@localhost
        DocumentRoot /Path/to/the/folder/sso_auth/wwwroot
        ServerAlias *.sso_auth.local
        <Directory "/Path/to/the/folder/sso_auth/wwwroot">
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Require all granted
        </Directory>

ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
<Directory "/usr/lib/cgi-bin">
    AllowOverride All
    Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
    Require all granted
</Directory>
        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
``````

