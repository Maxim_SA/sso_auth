<?php


namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use OAuth2\Server as OAuth2Server;
use OAuth2\HttpFoundationBridge\Response as BridgeResponse;
use OAuth2\Storage\Pdo;
use OAuth2\Storage\Memory;
use OAuth2\OpenID\GrantType\AuthorizationCode;
use OAuth2\GrantType\UserCredentials;
use OAuth2\GrantType\RefreshToken;

class OidcServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->singleton('App\OidcServer', function ($app) {
            $server = new \App\OidcServer();
            $server->setup();
            return $server;
        });
    }

}