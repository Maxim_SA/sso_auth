<?php


namespace App;

use OAuth2\Server as OAuth2Server;
use OAuth2\HttpFoundationBridge\Response as BridgeResponse;
use OAuth2\Storage\Pdo;
use OAuth2\Storage\Memory;
use OAuth2\OpenID\GrantType\AuthorizationCode;
use OAuth2\GrantType\UserCredentials;
use OAuth2\GrantType\RefreshToken;

class OidcServer
{
    /**
     * @var OAuth2Server
     */
    public $oauth2Server;

    public $counter = 0;

    public function setup()
    {
        $this->counter++;
        $dbName = env("DB_DATABASE");
        $storage = new PDO([
            "dsn" => "mysql:dbname={$dbName};host=localhost",
            "username" => env("DB_USERNAME"),
            "password" => env("DB_PASSWORD"),
        ]);

        // create array of supported grant types
        $grantTypes = [
            'authorization_code' => new AuthorizationCode($storage),
            'user_credentials' => new UserCredentials($storage),
            'refresh_token' => new RefreshToken($storage, ['always_issue_new_refresh_token' => true]),
        ];

        $server = new OAuth2Server(
            $storage,
            [
                'enforce_state' => true,
                'allow_implicit' => true,
                'use_openid_connect' => true,
                'issuer' => $_SERVER['HTTP_HOST'],
            ],
            $grantTypes
        );

        $server->addStorage($this->getKeyStorage(), 'public_key');
        $this->setOauth2Server($server);
    }

    public function getOauthResponse()
    {
        return new BridgeResponse();
    }

    private function getKeyStorage()
    {
        $publicKey = file_get_contents($this->getProjectRoot() . '/storage/server_keys/pubkey.pem');
        $privateKey = file_get_contents($this->getProjectRoot() . '/storage/server_keys/privkey.pem');

        // create storage
        $keyStorage = new Memory(['keys' => [
            'public_key' => $publicKey,
            'private_key' => $privateKey,
        ]]);

        return $keyStorage;
    }

    private function getProjectRoot()
    {
        return dirname(__DIR__);
    }

    /**
     * @return OAuth2Server
     */
    public function getOauth2Server()
    {
        return $this->oauth2Server;
    }

    /**
     * @param OAuth2Server $oauth2Server
     * @return \App\OidcServer
     */
    public function setOauth2Server($oauth2Server)
    {
        $this->oauth2Server = $oauth2Server;
        return $this;
    }


}