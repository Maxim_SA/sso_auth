<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;

class TokenController extends Controller
{
    /**
     *
     * @var \App\OidcServer
     */
    protected $authServer;

    public function __construct(\App\OidcServer $server)
    {
//        $server->setup();
        $this->authServer = $server;
    }

    /**
     * This is called by the client app once the client has obtained
     * an authorization code from the Authorize Controller
     * (see \App\Http\Controllers\AuthorizeController).
     * If the request is valid, an access token will be returned+
     * @param Request $request
     * @return
     */
    public function getAccessToken(Request $request)
    {
        file_put_contents("http.log", json_encode($_POST));
        $oauthRequest = \OAuth2\HttpFoundationBridge\Request::createFromGlobals();
        // let the oauth2-server-php library do all the work!
        $response = $this->authServer->getOauth2Server()->handleTokenRequest(
            $oauthRequest,
            $this->authServer->getOauth2Server()->getResponse()
        );
        return response()->json($response->getParameters(), $response->getStatusCode(), $response->getHttpHeaders());
    }
}