<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;

class AuthorizeController extends Controller
{
    /**
     *
     * @var \App\OidcServer
     */
    protected $authServer;

    public function __construct(\App\OidcServer $server)
    {
        $this->authServer = $server;
    }


    public function askUserAuthorization(Request $request)
    {
        $oauthRequest = \OAuth2\HttpFoundationBridge\Request::createFromGlobals();

        // validate the authorize request.  if it is invalid, redirect back to the client with the errors in tow
        if (!$this->authServer->getOauth2Server()->validateAuthorizeRequest($oauthRequest, $this->authServer->getOauthResponse())) {
            return $this->authServer->getOauth2Server()->getResponse();
        }

        // display the "do you want to authorize?" form
        return view(
            'authorize.authorize_form',
            [
                'client_id' => $request->get('client_id'),
                'response_type' => $request->get('response_type'),
                'query_string' => $request->getQueryString(),
            ]
        );
    }

    /**
     * This is called once the user decides to authorize or cancel the client app's
     * authorization request
     * @param Request $request
     * @return
     */
    public function authorizeFormSubmit(Request $request)
    {
        $oauthRequest = \OAuth2\HttpFoundationBridge\Request::createFromGlobals();
        // check the form data to see if the user authorized the request
        $authorized = (bool)$request->get('authorize');
        // call the oauth server and return the response
        return $this->authServer->getOauth2Server()->handleAuthorizeRequest(
            $oauthRequest,
            $this->authServer->getOauthResponse(),
            $authorized
        );
    }
}