<?php


namespace App\Http\Controllers;


use Symfony\Component\HttpFoundation\Response;

class ResourceController extends Controller
{
    /**
     *
     * @var \App\OidcServer
     */
    protected $authServer;

    public function __construct(\App\OidcServer $server)
    {
//        $server->setup();
        $this->authServer = $server;
    }

    /**
     * This is called by the client app once the client has obtained an access
     * token for the current user.  If the token is valid, the resource (in this
     * case, the "friends" of the current user) will be returned to the client
     */
    public function getFriends()
    {
        $oauthRequest = \OAuth2\HttpFoundationBridge\Request::createFromGlobals();
        if (!$this->authServer->getOauth2Server()->verifyResourceRequest(
            $oauthRequest,
            $this->authServer->getOauthResponse())
        ) {
            return $this->authServer->getOauth2Server()->getResponse();
        } else {
            // return a fake API response - not that exciting
            $apiResponse = [
                'friends' => [
                    'john',
                    'matt',
                    'jane',
                ],
            ];
            return response()->json($apiResponse);
        }
    }
}